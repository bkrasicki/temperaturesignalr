﻿using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Text;

namespace TemeratureSignalR
{
    public static class LedLogic
    {
        public const int PIN_YELLOW = 16;

        public static void LedStart(int pinNumber)
        {
            GpioController controller = new GpioController();
            controller.OpenPin(pinNumber, PinMode.Output);
            controller.Write(pinNumber, PinValue.High);
        }

        public static void LedStop(int pinNumber)
        {
            GpioController controller = new GpioController();
            controller.OpenPin(pinNumber, PinMode.Output);
            controller.Write(pinNumber, PinValue.Low);
        }
    }
}
