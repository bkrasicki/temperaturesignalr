﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace TemeratureSignalR
{
    public class SignalRClient
    {
        private readonly IConfiguration Configuration;
        private readonly HubConnection connection;

        public SignalRClient(IConfiguration configuration)
        {
            connection = new HubConnectionBuilder()
                .WithUrl(configuration["HubUrl"])
                .Build();
            Configuration = configuration;
        }

        public void Start()
        {
            connection.StartAsync();
            connection.On<int>("ChangeStateMassage", (tempStatus) =>
            {
                var status = tempStatus == 1 ? "on" : "off";
                Console.WriteLine($"Heating is {status}");
                if (tempStatus == 1)
                {
                    LedLogic.LedStart(LedLogic.PIN_YELLOW);
                } 
                else
                {
                    LedLogic.LedStop(LedLogic.PIN_YELLOW);
                }
            });
        }

        public void StopAsync()
        {
            connection.StopAsync().GetAwaiter().GetResult();
        }
    }
}
