﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using Unosquare.RaspberryIO;
using Unosquare.WiringPi;

namespace TemeratureSignalR
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            IConfiguration configuration = builder.Build();

            var client = new SignalRClient(configuration);
            client.Start();
            Pi.Init<BootstrapWiringPi>();
            TempLogic.TempSensor();
            client.StopAsync();
        }
    }
}
