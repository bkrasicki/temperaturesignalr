﻿using System;
using System.Collections.Generic;
using System.Text;
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Abstractions;
using Unosquare.RaspberryIO.Peripherals;

namespace TemeratureSignalR
{
    public static class TempLogic
    {

        public static void TempSensor()
        {
            Console.Clear();

            using var sensor = DhtSensor.Create(DhtType.Dht11, Pi.Gpio[BcmPin.Gpio04]);
            var totalReadings = 0.0;
            var validReadings = 0.0;

            sensor.Start();

            sensor.OnDataAvailable += (s, e) =>
            {
                totalReadings++;
                if (!e.IsValid)
                    return;
                validReadings++;
                Console.WriteLine($"Temperature: \n {e.Temperature:0.00}°C");
                Console.WriteLine($"Humidity: \n {e.HumidityPercentage:P0}");
            };
            while (true)
            {
                var input = Console.ReadKey(true).Key;
                if (input != ConsoleKey.Escape) continue;

                break;
            }
        }

    }
}
